/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.animals;

/**
 *
 * @author WIP
 */
public class Animal {
    protected String name;
    protected int legs = 0;
    protected String color;
    
    public Animal(String name, String color, int legs){
        System.out.println("Animal created");
        this.name = name;
        this.color = color;
        this.legs = legs;
    }
    public void walk(){
        System.out.println("Animal walk");
    }
    
    public void speak(){
        System.out.println("Animal speak");
        System.out.println("name: " + this.name + " color: " + this.color 
                + " legs: " + this.legs);
    }

    public String getName() {
        return name;
    }

    public int getLegs() {
        return legs;
    }

    public String getColor() {
        return color;
    }
    
    
}
