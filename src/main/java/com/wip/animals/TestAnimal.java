/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.animals;

/**
 *
 * @author WIP
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Jojo", "White", 0);
        animal.speak();
        animal.walk();
        
        System.out.println("");
        //1
        Dog Bo = new Dog("Bo", "Brown");
        Bo.speak();
        Bo.walk();
        
        System.out.println("");
        //2
        Dog Big = new Dog("Big", "Black");
        Big.speak();
        Big.walk();
        
        System.out.println("");
        //3
        Dog Ben = new Dog("Ben", "White");
        Ben.speak();
        Ben.walk();
        
        System.out.println("");
        //4
        Dog Bison = new Dog("Bison", "Gray");
        Bison.speak();
        Bison.walk();
        
        System.out.println("");
        //5
        Cat Garfield = new Cat("Garfield", "Orange");
        Garfield.speak();
        Garfield.walk();
        
        System.out.println("");
        //6
        Duck Kubkub = new Duck("Kubkub", "Yellow");
        Kubkub.speak();
        Kubkub.walk();
        Kubkub.fly();
        
        System.out.println("");
        //7
        Duck Kipkip = new Duck("Kipkip", "Orange");
        Kipkip.speak();
        Kipkip.walk();
        Kipkip.fly();
        
        System.out.println("");
        
        System.out.println("Kubkub is Animal: " + (Kubkub instanceof Animal));
        System.out.println("Kubkub is Duck: " + (Kubkub instanceof Duck));
        System.out.println("Kubkub is Object: " + (Kubkub instanceof Object));
        System.out.println("Animal is Dog: " + (animal instanceof Dog));
        System.out.println("Animal is Cat: " + (animal instanceof Cat));
        System.out.println("Bo is Animal: " + (Bo instanceof Animal));
        System.out.println("Bo is Dog: " + (Bo instanceof Dog));
        System.out.println("Bo is Object: " + (Bo instanceof Object));
        System.out.println("Garfield is Animal: " + (Garfield instanceof Animal));
        System.out.println("Garfield is Cat: " + (Garfield instanceof Cat));
        System.out.println("Garfield is Object: " + (Garfield instanceof Object));
        
        System.out.println("");
        
        Animal[] animals = {Bo, Big, Ben, Bison, Garfield, Kubkub, Kipkip};
        for(int i=0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            System.out.println("");
        }
        
    }
}
